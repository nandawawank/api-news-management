<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogUpdatedNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_log_updated_news', function (Blueprint $table) {
            $table->id();
            $table->integer('updated_by');
            $table->integer('news_id');
            $table->string('title_before');
            $table->string('content_before');
            $table->string('image_before');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_log_updated_news');
    }
}
