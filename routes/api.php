<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\NewsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login'])->name('login');
Route::post('/user/store', [UserController::class, 'store']);
Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/news', [NewsController::class, 'index']);
    Route::get('/news/detail', [NewsController::class, 'detail']);
    Route::post('/news/store', [NewsController::class, 'store']);
    Route::post('/news/delete', [NewsController::class, 'delete']);
    Route::post('/news/update', [NewsController::class, 'update']);
    Route::post('/news/comment', [NewsController::class, 'comment']);
    Route::post('/logout', [UserController::class, 'logout']);
});
