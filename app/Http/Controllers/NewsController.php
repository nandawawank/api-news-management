<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\UpdatedNewsLog;
use App\Models\CreatedNewsLog;
use App\Models\DeletedNewsLog;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Jobs\CommentQueue;

use App\Http\Resources\GetNews as GetNewsResource;
use App\Http\Resources\Validator as ValidatorResource;
use App\Http\Resources\StoreNewsLog as CreateNewsResource;
use App\Http\Resources\DeleteNewsLog as DeleteNewsResource;
use App\Http\Resources\UpdateNewsLog as UpdatedNewsResource;

class NewsController extends Controller
{
    public $successStatus = 200;
    public $uploadPath = 'image_news';

    public function index(Request $request)
    {
        $pages = 10;
        $get_data = '';
        $count_data = News::count();

        if ($request->input('title') != '' || $request->input('title') != null) {
            $get_data = News::where('title_news', '=', $request->input('title'))
            ->select('title_news','image_name','image_path','updated_at')
            ->paginate($pages);
        } else {
            $get_data = News::select('title_news','image_name','image_path','updated_at')->paginate($pages);
        }

        return new GetNewsResource($get_data);
    }

    public function detail(Request $request)
    {

        $get_data = [];
        
        if ($request->input('news_id') != '' || $request->input('news_id') != null) {
            $get_data['news'] = News::find($request->input('news_id'))->first();
            $get_data['comments'] = News::find($request->input('news_id'))->comments()->first();
        }

        return new GetNewsResource($get_data);
    }

    public function comment(Request $request)
    {
        $data = [
            'news_id'   => $request->input('news_id'),
            'user_id'   => session()->get('user_id'),
            'comment'   => $request->input('comment')
        ];
        // return response()->json($data);
        $comment = CommentQueue::dispatch($data);
    }

    public function store(Request $request)
    {
        $rules = [
            'title_news'    => 'required',
            'content_news'  => 'required',
            'image'         => 'required',
        ];

        $messages = [
            'title_news.required'   => 'A title news is required',
            'content_news.required' => 'A content news is required',
            'image.required'        => 'A image is required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = array($validator->messages());
            return new ValidatorResource($errors);
        }

        if ($request->isMethod('post')) {

            if (session()->get('privilage') != 'admin') {
                $errors = array(['errors' => 'unauthorized']);
                return new ValidatorResource($errors);
            }

            $image_file = $request->file('image');
            $image_file->move($this->uploadPath, $image_file->getClientOriginalName());
            
            $create_news = new News;
            $create_news->created_by = session()->get('user_id');
            $create_news->title_news = $request->input('title_news');
            $create_news->content_news = $request->input('content_news');
            $create_news->image_name = $image_file->getClientOriginalName();
            $create_news->image_path = $this->uploadPath;
            $create_news->save();

            if ($create_news) {

                $create_log = new CreatedNewsLog;
                $create_log->created_by = session()->get('user_id');
                $create_log->title_news = $request->input('title_news');
                $create_log->save();

                return new CreateNewsResource($create_news);
            } else {
                return new CreateNewsResource($create_news);
            }
        } else {
            return new CreateNewsResource(NULL);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'id'            => 'required',
            'title_news'    => 'required',
            'content_news'  => 'required',
            'image'         => 'required',
        ];

        $messages = [
            'title_news.required'   => 'A title news is required',
            'content_news.required' => 'A content news is required',
            'image.required'        => 'A image is required',
            'id.required'           => 'A id is required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = array($validator->messages());
            return new ValidatorResource($errors);
        }

        if (session()->get('privilage') != 'admin') {
            $errors = array(['errors' => 'unauthorized']);
            return new ValidatorResource($errors);
        }

        if ($request->isMethod('post')) {
            
            $image_file = $request->file('image');
            $image_file->move($this->uploadPath, $image_file->getClientOriginalName());

            $news_before = News::find($request->input('id'));
            $update_news = News::find($request->input('id'));
            $update_news->created_by = session()->get('user_id');
            $update_news->title_news = $request->input('title_news');
            $update_news->content_news = $request->input('content_news');
            $update_news->image_name = $image_file->getClientOriginalName();
            $update_news->image_path = $this->uploadPath;
            $update_news->save();

            if ($update_news) {
                $update_log = new UpdatedNewsLog;
                $update_log->updated_by = session()->get('user_id');
                $update_log->news_id = $news_before->id;
                $update_log->title_before = $news_before->title_news;
                $update_log->content_before = $news_before->content_news;
                $update_log->image_before = $news_before->image_name;
                $update_log->save();

                return new UpdatedNewsResource($update_news);
            } else {
                return new UpdatedNewsResource($update_news);
            }
        } else {
            return new UpdatedNewsResource(NULL);
        }
    }

    public function delete(Request $request)
    {
        $rules = [
            'id' => 'required'
        ];

        $messages = [
            'id.required'   => 'A parameter id required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            $errors = array($validator->messages());
            return new ValidatorResource($errors);
        }

        if (session()->get('privilage') != 'admin') {
            $errors = array(['errors' => 'unauthorized']);
            return new ValidatorResource($errors);
        }

        if ($request->isMethod('post')) {

            $delete_data = News::find($request->input('id'));
            $delete_news = News::destroy($request->input('id'));

            if ($delete_news) {
                
                $delete_log = new DeletedNewsLog;
                $delete_log->deleted_by = session()->get('user_id');
                $delete_log->title_deleted = $delete_data->title_news;
                $delete_log->content_deleted = $delete_data->content_news;
                $delete_log->image_deleted = $delete_data->image_name;
                $delete_log->save();
                
                return new DeleteNewsResource($delete_log);
            } else {
                return new DeleteNewsResource($delete_log);
            }
        } else {
            return new DeleteNewsResource(NULL);
        }
    }
}
