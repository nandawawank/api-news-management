<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\Validator as ValidatorResource;
use App\Http\Resources\LoginUser as LoginResource;
use App\Http\Resources\StoreUser as StoreResource;

class UserController extends Controller
{
    // client id 1 => h41OZDpI7Ub5NQF0O4bCQpL7KkhyUqLGSWjmZrDi
    // clieatn id 2 => occj4WqyPufnCVJ0BCK87r878EQksDudopMLMugN

    public $successStatus = 200;

    public function store(Request $request) 
    {
        if ($request->isMethod('post')) {
            $rules = [
                'name'      => 'required|max:255',
                'email'     => 'required|unique:users|max:255',
                'password'  => 'required|min:8'
            ];

            $messages = [
                'name.required'     => 'A name is required',
                'email.required'    => 'A email is required',
                'password.required' => 'A password is required',
                'name.max'          => 'exceeds the maximum character (max : 255)',
                'password.min'      => 'less than the required minimum number of characters (min : 8)'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                $errors = array($validator->messages());
                return new ValidatorResource($errors);
            }

            $create_user = new User;
            $create_user->name = $request->input('name');
            $create_user->email = $request->input('email');
            $create_user->password = Hash::make($request->input('password'));
            $create_user->privilage = $request->input('privilage');
            $create_user->save();

            if ($create_user) {
                return new StoreResource($create_user);
            } else {
                return new StoreResource($create_user);
            }
        } else {
            return new StoreResource(NULL);
        }
    }

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'email'     => 'required',
                'password'  => 'required'
            ];

            $messages = [
                'email.required'    => 'A email is required',
                'password.required' => 'A password is reuired'
            ];

            $validator = Validator::make($request->only(['email', 'password']), $rules, $messages);

            if ($validator->fails()) {
                $errors = array($validator->messages());
                return new ValidatorResource($errors);
            }

            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();
                $success['token'] =  $user->createToken($request->input('email'))->accessToken;

                $update_token = User::where('email', '=', $request->input('email'))->first();

                session()->put('user_id', $update_token->id);
                session()->put('privilage', $update_token->privilage);

                $update_token->api_token = $success['token'];
                $update_token->save();
                
                $success = array($success);
                return new LoginResource($success);
            } else {
                $fail = array(['error' => 'Unauthorised']);
                return new LoginResource($fail);
            }
        }
    }

    public function logout(Request $request)
    {
        $logout_user = User::find(session()->get('user_id'));
        $logout_user->api_token = null;
        $logout_user->save();

        session()->flush();
        $logout = $request->user()->token()->revoke();
        
        if($logout){
            $messages = array(['message' => 'Successfully logged out']);
            return new LoginResource($messages);
        } else {
            $fail = array(['error' => 'Logout fail']);
            return new LoginResource($fail);
        }
    }
}
