<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreatedNewsLog extends Model
{
    use HasFactory;

    protected $table = "t_log_created_news";

    protected $fillable = [
        'created_by',
        'title_news'
    ];
}
