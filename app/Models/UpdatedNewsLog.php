<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpdatedNewsLog extends Model
{
    use HasFactory;

    protected $table = "t_log_updated_news";

    protected $fillable = [
        'updated_by',
        'news_id',
        'title_before',
        'content_before',
        'image_before'
    ];
}
