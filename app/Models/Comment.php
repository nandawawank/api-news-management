<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $table = "t_comment";

    protected $fillable = [
        'news_id',
        'user_id',
        'comment'
    ];
}
