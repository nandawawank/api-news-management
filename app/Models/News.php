<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $table = "t_news";

    protected $fillable = [
        'created_by',
        'title_news',
        'content_news',
        'image_name',
        'image_path'
    ];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
