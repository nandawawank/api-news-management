<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeletedNewsLog extends Model
{
    use HasFactory;

    protected $table = "t_log_deleted_news";

    protected $fillable = [
        'deleted_by',
        'title_deleted',
        'content_deleted',
        'image_deleted'

    ];
}
