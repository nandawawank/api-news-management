<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Comment;

class CommentQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $create_comment = new Comment;
        $create_comment->news_id = $this->data['news_id'];
        $create_comment->user_id = $this->data['user_id'];
        $create_comment->comment = $this->data['comment'];
        $create_comment->save();

        if ($create_comment) {
            echo "Comment created";
        } else {
            echo "Comment uncreated";
        }
    }
}
